﻿using System;
using System.Collections.Generic;

namespace Z.FarmHelper.Schemas {
	public class CardsFarmer {
		public List<Game> CurrentGamesFarming = new List<Game>();
		public List<Game> GamesToFarm = new List<Game>();
		public bool Paused;
		public TimeSpan TimeRemaining;
	}
}
