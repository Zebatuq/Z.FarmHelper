﻿using System.Collections.Generic;
using Z.FarmHelper.Enums;

namespace Z.FarmHelper.Schemas {
	public class BotConfig {
		public bool AcceptGifts;
		public bool AutoSteamSaleEvent;
		public BotBehaviour BotBehaviour;
		public string CustomGamePlayedWhileFarming;
		public bool CustomGamePlayedWhileIdle;
		public bool DismissInventoryNotifications;
		public bool Enabled;
		public FarmingOrder FarmingOrder;

		public List<FarmingOrders> FarmingOrders;
		public bool FarmOffline;
		public bool FastFarming;
		public List<int> GamesPlayedWhileIdle = new List<int>();
		public int HoursUntilCardDrops;
		public bool IdlePriorityQueueOnly;
		public bool IdleRefundableGames;
		public bool IsBotAccount;
		public List<ItemsTypes> LootableTypes = new List<ItemsTypes>();
		public List<ItemsTypes> MatchableTypes = new List<ItemsTypes>();
		public PasswordFormat PasswordFormat;
		public bool Paused;
		public string s_SteamMasterClanID;
		public bool SendOnFarmingFinished;
		public int SendTradePeriod;
		public bool ShutdownOnFarmingFinished;
		public string SteamLogin;
		public ulong SteamMasterClanID;
		public string SteamParentalCode;
		public string SteamParentalPIN;
		public string SteamPassword;
		public string SteamTradeToken;
		public Dictionary<ulong, SteamUserPermissions> SteamUserPermissions;
		public TradingPreferences TradingPreferences;
		public List<ItemsTypes> TransferableTypes = new List<ItemsTypes>();
		public bool UseLoginKeys;
	}
}
