﻿using System.Collections.Generic;
using Z.FarmHelper.Enums;

namespace Z.FarmHelper.Schemas {
	public class GlobalConfig {
		public bool AutoRestart;
		public List<int> Blacklist = new List<int>();
		public string CommandPrefix;
		public int ConfirmationsLimiterDelay;
		public int ConnectionTimeout;
		public string CurrentCulture;
		public bool Debug;
		public bool DoSaleTasks;
		public int FarmingDelay;
		public int GiftsLimiterDelay;
		public bool Headless;
		public int IdleFarmingPeriod;
		public int InventoryLimiterDelay;
		public bool IPC;
		public string IPCHost;
		public string IPCPassword;
		public int IPCPort;
		public List<string> IPCPrefixes;
		public int LoginLimiterDelay;
		public int MaxFarmingTime;
		public int MaxTradeHoldDuration;
		public OptimizationMode OptimizationMode;
		public string s_SteamOwnerID;
		public int SaleEventThreadLimit;
		public bool Statistics;
		public string SteamMessagePrefix;
		public ulong SteamOwnerID;
		public SteamProtocols SteamProtocols;
		public UpdateChannel UpdateChannel;
		public int UpdatePeriod;
		public bool UseCustomMachineID;
		public int WebLimiterDelay;
		public string WebProxy;
		public string WebProxyPassword;
		public string WebProxyUsername;
	}
}
