﻿namespace Z.FarmHelper.Schemas {
	public class GenericResponse {
		public string Message;
		public bool Success;
	}
}
