﻿namespace Z.FarmHelper.Schemas {
	public class Game {
		public int AppID;
		public int CardsRemaining;
		public string GameName;
		public float HoursPlayed;
	}
}
