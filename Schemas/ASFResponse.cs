﻿using System;

namespace Z.FarmHelper.Schemas {
	public class ASFResponse {
		public string BuildVariant;
		public GlobalConfig GlobalConfig;
		public int MemoryUsage;
		public string ProcessStartTime;
		public Version Version;
		public bool VSF;
	}
}
