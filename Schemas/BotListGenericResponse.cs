﻿using System.Collections.Generic;
using Z.FarmHelper.Class;

namespace Z.FarmHelper.Schemas {
	public class BotListGenericResponse : GenericResponse {
		public Dictionary<string, Bot> Result;
	}
}
