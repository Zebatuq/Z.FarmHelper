﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Z.FarmHelper.Class;
using Z.FarmHelper.Schemas;

namespace Z.FarmHelper.Forms {
	public partial class MainWindow : INotifyPropertyChanged {
		private static readonly object locker = new object();

		public MainWindow() {
			Configuration.Load();
			Servers.Load();
			Servers.Save();

			Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(Configuration.Config.Lang);
			InitializeComponent();
			DataContext = this;
			Task.Run(UpdateServerInfo);
		}

		public int ServersResponded { get; set; }
		public int GamesLeftProperties { get; set; }
		public int CardsLeftProperties { get; set; }
		public double SecondsLeft { get; set; }
		public string TimeLeftProperties => TimeSpan.FromSeconds(SecondsLeft).ToString("h'h 'm'min'");
		public string ServersRespondedProperties => $"{ServersResponded}/{Servers.GetActive().Count}";
		public event PropertyChangedEventHandler PropertyChanged;

		private void ButtonOpenSettingsClick(object sender, RoutedEventArgs e) {
			SettingsForm settingsForm = new SettingsForm();
			settingsForm.ShowDialog();
		}

		private void UpdateServerInfo() {
			Servers.GetActive().ForEach(async server => {
				ASFResponseGenericResponse res = await server.API.SendAPI<ASFResponseGenericResponse>("ASF");
				if (!res.Success) {
					return;
				}

				BotListGenericResponse resBots = await server.API.GetBots();
				if (!resBots.Success) {
					return;
				}

				SetInfo(resBots);
			});
		}

		private void SetInfo(BotListGenericResponse resBots) {
			lock (locker) {
				ServersResponded++;
				resBots.Result.ToList().ForEach(bot => GamesLeftProperties += bot.Value.CardsFarmer.GamesToFarm.Count);
				resBots.Result.ToList().ForEach(bot => CardsLeftProperties += bot.Value.CardsFarmer.GamesToFarm.Sum(game => game.CardsRemaining));

				TimeSpan time = TimeSpan.FromSeconds(resBots.Result.ToList().Max(bot => Convert.ToInt32(bot.Value.CardsFarmer.TimeRemaining.TotalSeconds)));
				SecondsLeft = SecondsLeft < time.TotalSeconds ? time.TotalSeconds : SecondsLeft;
			}
		}

		private void ButtonDonateClick(object sender, RoutedEventArgs e) {
			Process.Start("https://zebatuq.ru");
		}

		private void ButtonFullStatusClick(object sender, RoutedEventArgs e) {
			StatusForm statusForm = new StatusForm();
			statusForm.Show();
		}
	}
}
