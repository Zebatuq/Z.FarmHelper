﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using RestSharp;
using Z.FarmHelper.Class;
using Z.FarmHelper.Enums;
using Z.FarmHelper.Schemas;

namespace Z.FarmHelper.Forms {
	public partial class StatusForm : INotifyPropertyChanged {
		public static ObservableCollection<StatusServer> StatusServersList;
		public static ObservableCollection<StatusAllServers> StatusAllServersList = new ObservableCollection<StatusAllServers>();

		public StatusForm() {
			InitializeComponent();
			DataContext = this;
			UpdateServers();
		}

		public int TextSize {
			set => Configuration.Config.StatusTextSize = value;
			get => Configuration.Config.StatusTextSize;
		}

		public event PropertyChangedEventHandler PropertyChanged;

		private async void UpdateServers() {
			ButtonUpdate.IsEnabled = false;

			StatusServersList = new ObservableCollection<StatusServer>(Servers.GetActive().ConvertAll(x => new StatusServer(x.Name, x.Address, x.Password)));
			DataGridServers.ItemsSource = StatusServersList;

			StatusAllServersList = new ObservableCollection<StatusAllServers> {
				new StatusAllServers()
			};
			DataGridAllServers.ItemsSource = StatusAllServersList;

			await Task.WhenAll(StatusServersList.Select(GetUpdate));

			ButtonUpdate.IsEnabled = true;
		}

		private async Task GetUpdate(StatusServer server) {
			ASFResponseGenericResponse res = await server.API.SendAPI<ASFResponseGenericResponse>("ASF");
			if (!res.Success) {
				server.Status = "ERROR";
				server.StatusError = res.Message;
				return;
			}

			server.Status = "OK";
			server.Version = res.Result.VSF ? res.Result.Version + " ♥" : res.Result.Version.ToString();
			server.RAM = res.Result.MemoryUsage / 1024 + " МБ";

			BotListGenericResponse resBots = await server.API.GetBots();
			if (!resBots.Success) {
				server.Status = "ERROR";
				server.StatusError = resBots.Message;
				return;
			}

			server.BotList = resBots.Result.Values.ToList();
			server.Online = true;

			DataGridServers.Items.Refresh();
			DataGridAllServers.Items.Refresh();
		}

		private void ButtonUpdateClick(object sender, RoutedEventArgs e) {
			UpdateServers();
		}

		private async void DataGridServersContextMenuOpen(object sender, MouseButtonEventArgs e) {
			if (DataGridServers.ContextMenu == null) {
				return;
			}

			DataGridServers.ContextMenu.Items.Clear();
			await Task.Delay(1);
			List<StatusServer> servers = DataGridServers.SelectedItems.Cast<StatusServer>().ToList();

			if (servers.Count <= 0) {
				return;
			}

			MenuItem serverName = new MenuItem {
				IsEnabled = false
			};

			if (servers.Count == 1) {
				serverName.Header = servers[0].Name;
			} else {
				serverName.Header = "Серверов: " + servers.Count(x => x.Online);
			}

			DataGridServers.ContextMenu.Items.Add(serverName);

			if (servers.Count == 1) {
				MenuItem openLog = new MenuItem {
					Header = "Открыть лог"
				};
				//openLog.Click += OpenConsole;
				DataGridServers.ContextMenu.Items.Add(openLog);
			}

			if (servers.Count == 1) {
				MenuItem editAsfJson = new MenuItem {
					Header = "Редактировать ASF.json"
				};
				//editAsfJson.Click += EditAsfJson;
				DataGridServers.ContextMenu.Items.Add(editAsfJson);
			}

			MenuItem updateASF = new MenuItem {
				Header = Properties.Resources.Update
			};
			updateASF.Click += (s, r) => SelectedServersUpdate(servers);
			DataGridServers.ContextMenu.Items.Add(updateASF);

			MenuItem restartASF = new MenuItem {
				Header = Properties.Resources.Restart
			};
			restartASF.Click += (s, r) => SelectedServersRestart(servers);
			DataGridServers.ContextMenu.Items.Add(restartASF);

			DataGridServers.ContextMenu.IsOpen = true;
		}

		private void SelectedServersUpdate(List<StatusServer> servers) {
			servers.ForEach(
				async server => {
					GenericResponse res = await server.API.SendAPI<GenericResponse>("ASF/Update", Method.POST);
					if (res.Success) {
						server.Online = false;
						server.Status = Properties.Resources.Update;
						DataGridServers.Items.Refresh();
						DataGridAllServers.Items.Refresh();
					}
				}
			);
			DataGridServers.SelectedIndex = -1;
		}

		private void SelectedServersRestart(List<StatusServer> servers) {
			servers.ForEach(
				async server => {
					GenericResponse res = await server.API.SendAPI<GenericResponse>("ASF/Restart", Method.POST);
					if (res.Success) {
						server.Status = Properties.Resources.Restart;
						server.Online = false;
						DataGridServers.Items.Refresh();
						DataGridAllServers.Items.Refresh();
					}
				}
			);
			DataGridServers.SelectedIndex = -1;
		}

		public class StatusServer : Server {
			public List<Bot> BotList = new List<Bot>();

			public StatusServer(string name, string address, string password) : base(name, address, password) {
			}

			public bool Online { set; get; }

			public string Status { set; get; }
			public string RAM { set; get; }
			public string Version { set; get; }
			public string StatusError { set; get; }

			public int CardsLeft => BotList.Sum(bot => bot.CardsFarmer.GamesToFarm.Count);
			public int GamesLeft => BotList.Sum(bot => bot.CardsFarmer.GamesToFarm.Sum(game => game.CardsRemaining));
			public int Idle => BotList.Count(bot => bot.Status == BotStatus.Idle);
			public int Paused => BotList.Count(bot => bot.Status == BotStatus.Paused);
			public int Connecting => BotList.Count(bot => bot.Status == BotStatus.Connecting);
			public int NotRunning => BotList.Count(bot => bot.Status == BotStatus.NotRunning);
			public int Limited => BotList.Count(bot => bot.Status == BotStatus.Limited);
			public int Locked => BotList.Count(bot => bot.Status == BotStatus.Locked);
			public int Farming => BotList.Count(bot => bot.Status == BotStatus.Farming);
		}

		public class StatusAllServers {
			public string OnlineStatus => StatusServersList.Count(x => x.Online) + "/" + StatusServersList.Count;
			public int Idle => StatusServersList.Sum(x => x.Idle);
			public int Paused => StatusServersList.Sum(x => x.Paused);
			public int CardsLeft => StatusServersList.Sum(x => x.GamesLeft);
			public int GamesLeft => StatusServersList.Sum(x => x.CardsLeft);
			public int Connecting => StatusServersList.Sum(x => x.Connecting);
			public int NotRunning => StatusServersList.Sum(x => x.NotRunning);
			public int Limited => StatusServersList.Sum(x => x.Limited);
			public int Locked => StatusServersList.Sum(x => x.Locked);
			public int Farm => StatusServersList.Sum(x => x.Farming);
		}
	}
}
