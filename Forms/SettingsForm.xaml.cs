﻿using System.Windows;
using System.Windows.Controls;
using Z.FarmHelper.Class;

namespace Z.FarmHelper.Forms {
	public sealed partial class SettingsForm {
		public SettingsForm() {
			InitializeComponent();
			DataGridServers.ItemsSource = Servers.ServersList;
			DataGridServers.Items.Refresh();
			ComboBoxName.SelectedIndex = Configuration.Config.Lang == "ru-RU" ? 0 : 1;
		}

		private void ButtonServerAddClick(object sender, RoutedEventArgs e) {
			string name = TextBoxName.Text;
			string address = TextBoxAddress.Text;
			string password = TextBoxPassword.Text;

			if ((name.Length == 0) || (address.Length == 0) || (password.Length == 0)) {
				return;
			}

			if ((Servers.GetServerByAddress(address) != null) || (Servers.GetServerByName(name) != null)) {
				//Todo Окно ошибки
				return;
			}

			Servers.ServersList.Add(new Server(name, address, password));

			TextBoxName.Text = "";
			TextBoxAddress.Text = "";
			TextBoxPassword.Text = "";
		}

		private void ComboBoxNameSelectionChanged(object sender, SelectionChangedEventArgs e) {
			Configuration.Config.Lang = ComboBoxName.SelectedIndex == 0 ? "ru-RU" : "en-US";
		}
	}
}
