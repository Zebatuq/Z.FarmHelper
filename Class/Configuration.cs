﻿using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Windows;
using Newtonsoft.Json;

namespace Z.FarmHelper.Class {
	public static class Configuration {
		public static ConfigurationData Config;

		public static void Load() {
			Config = ConfigurationData.LoadConfig();
		}

		internal static void Restart() {
			Process.Start(Application.ResourceAssembly.Location);
			Application.Current.Shutdown();
		}

		public sealed class ConfigurationData {
			private static string _lang;
			private static int _statusTextSize = 10;

			[JsonProperty]
			public string Lang {
				get => _lang;
				set {
					if (_lang == value) {
						return;
					}

					if (string.IsNullOrEmpty(_lang)) {
						_lang = value;
						SaveConfig();
						return;
					}

					_lang = value;
					SaveConfig();
					Restart();
				}
			}

			[JsonProperty]
			public int StatusTextSize {
				set {
					if (_statusTextSize == value) {
						return;
					}

					_statusTextSize = value;
					SaveConfig();
				}
				get => _statusTextSize;
			}

			public static ConfigurationData LoadConfig() {
				ConfigurationData config;
				if (!File.Exists("config.json")) {
					config = CreateAndSaveConfig();
				} else {
					try {
						string content = File.ReadAllText("config.json");
						config = JsonConvert.DeserializeObject<ConfigurationData>(content);
					} catch (JsonException) {
						config = CreateAndSaveConfig();
					}
				}

				return config;
			}

			private static ConfigurationData CreateAndSaveConfig() {
				ConfigurationData config = new ConfigurationData {
					Lang = GetSystemLang()
				};

				config.SaveConfig();
				return config;
			}

			private static string GetSystemLang() {
				string localeCode = CultureInfo.CurrentUICulture.IetfLanguageTag;

				if (localeCode.Contains("-")) {
					localeCode = localeCode.Remove(localeCode.IndexOf('-'));
				}

				return (localeCode == "ru") || (localeCode == "be") || (localeCode == "ua") ? "ru-RU" : "en-US";
			}

			private void SaveConfig() {
				string content = JsonConvert.SerializeObject(this, Formatting.Indented);
				File.WriteAllText("config.json", content);
			}
		}
	}
}
