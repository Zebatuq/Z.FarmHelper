﻿using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RestSharp;
using Z.FarmHelper.Schemas;

namespace Z.FarmHelper.Class {
	public class API {
		private readonly RestClient _client;
		private readonly Server _server;

		public API(Server server) {
			_server = server;
			_client = new RestClient($"http://{_server.Address}");
		}

		public async Task<T> SendAPI<T>(string req, Method met = Method.GET) where T : GenericResponse, new() {
			RestRequest request = new RestRequest($"/API/{req}", met);
			request.AddHeader("Authentication", _server.Password);

			IRestResponse res = await _client.ExecuteTaskAsync(request);
			if (res.StatusCode == 0) {
				return new T {
					Message = res.ErrorMessage
				};
			}

			if (res.StatusCode == (HttpStatusCode) 400) {
				return JsonConvert.DeserializeObject<T>(res.Content);
			}

			if (res.StatusCode != (HttpStatusCode) 200) {
				return new T {
					Message = res.ErrorMessage
				};
			}

			return JsonConvert.DeserializeObject<T>(res.Content);
		}

		public async Task<BotListGenericResponse> GetBots() {
			RestRequest request = new RestRequest("/API/BOT/ASF", Method.GET);
			request.AddHeader("Authentication", _server.Password);

			IRestResponse resa = await _client.ExecuteTaskAsync(request);

			if (resa.StatusCode == 0) {
				return new BotListGenericResponse {
					Message = resa.ErrorMessage
				};
			}

			if (resa.StatusCode == (HttpStatusCode) 400) {
				return JsonConvert.DeserializeObject<BotListGenericResponse>(resa.Content);
			}

			if (resa.StatusCode != (HttpStatusCode) 200) {
				return new BotListGenericResponse {
					Message = resa.ErrorMessage
				};
			}

			return JsonConvert.DeserializeObject<BotListGenericResponse>(resa.Content);
		}
	}
}
