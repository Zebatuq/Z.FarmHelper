﻿using Z.FarmHelper.Enums;
using Z.FarmHelper.Schemas;

namespace Z.FarmHelper.Class {
	public class Bot {
		public AccountFlags AccountFlags;
		public string AvatarHash;
		public BotConfig BotConfig;
		public string BotName;
		public CardsFarmer CardsFarmer;
		public int GamesToRedeemInBackgroundCount;
		public bool IsConnectedAndLoggedOn;
		public bool IsPlayingPossible;
		public bool KeepRunning;
		public string Nickname;
		public string s_SteamID;
		public ulong SteamID;
		public ulong WalletBalance;
		public WalletCurrency WalletCurrency;

		public BotStatus Status {
			get {
				if (!KeepRunning) {
					return BotStatus.NotRunning;
				}

				if (!IsConnectedAndLoggedOn) {
					return BotStatus.Connecting;
				}

				if (CardsFarmer.Paused) {
					return BotStatus.Paused;
				}

				if (AccountFlags.HasFlag(AccountFlags.Lockdown)) {
					return BotStatus.Locked;
				}

				if (AccountFlags.HasFlag(AccountFlags.LimitedUser) || AccountFlags.HasFlag(AccountFlags.LimitedUserForce)) {
					return BotStatus.Limited;
				}

				return CardsFarmer.CurrentGamesFarming.Count == 0 ? BotStatus.Idle : BotStatus.Farming;
			}
		}
	}
}
