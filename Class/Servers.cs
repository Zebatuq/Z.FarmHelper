﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using Newtonsoft.Json;

namespace Z.FarmHelper.Class {
	public static class Servers {
		public static ObservableCollection<Server> ServersList = new ObservableCollection<Server>();

		public static Server GetServerByName(string name) {
			return ServersList.FirstOrDefault(server => server.Name == name);
		}

		public static Server GetServerByAddress(string address) {
			return ServersList.FirstOrDefault(server => server.Address == address);
		}

		public static List<Server> GetActive() {
			return ServersList.ToList().FindAll(x => x.Active);
		}

		public static void Load() {
			if (!File.Exists("servers.json")) {
				return;
			}

			try {
				string serversContent = File.ReadAllText("Servers.json");
				ServersList = JsonConvert.DeserializeObject<ObservableCollection<Server>>(serversContent);
			} catch (JsonException) {
				// TODO: стактрейс форма вывода ошибок
			} finally {
				ServersList.CollectionChanged += SaveOnChanged;
			}
		}

		public static void Save() {
			string serversContent = JsonConvert.SerializeObject(ServersList, Formatting.Indented);
			File.WriteAllText("Servers.json", serversContent);
		}

		public static void SaveOnChanged(object sender, NotifyCollectionChangedEventArgs e) {
			Save();
		}

		public static void SaveOnChanged(object sender, PropertyChangedEventArgs e) {
			Save();
		}
	}
}
