﻿using System.ComponentModel;
using Newtonsoft.Json;

namespace Z.FarmHelper.Class {
	public class Server : INotifyPropertyChanged {
		[JsonIgnore]
		public API API;

		public Server(string name, string address, string password) {
			Name = name;
			Address = address;
			Password = password;
			API = new API(this);

			if (!GetType().IsSubclassOf(typeof(Server))) {
				PropertyChanged += Servers.SaveOnChanged;
			}
		}


		[JsonProperty]
		public string Name { set; get; }

		[JsonProperty]
		public string Address { set; get; }

		[JsonProperty]
		public string Password { set; get; }

		[JsonProperty]
		public bool Active { set; get; } = true;

		public event PropertyChangedEventHandler PropertyChanged;
	}
}
