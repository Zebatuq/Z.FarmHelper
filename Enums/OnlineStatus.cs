﻿namespace Z.FarmHelper.Enums {
	public enum OnlineStatus : byte {
		Offline,
		Online,
		Busy,
		Away,
		Snooze,
		LookingToTrade,
		LookingToPlay,
		Invisible,
		Max
	}
}
