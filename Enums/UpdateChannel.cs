﻿namespace Z.FarmHelper.Enums {
	public enum UpdateChannel {
		None,
		Stable,
		Experimental
	}
}
