﻿using System;

namespace Z.FarmHelper.Enums {
	[Flags]
	public enum BotStatus : byte {
		None = 0,
		Idle = 1,
		Farming = 2,
		Paused = 4,
		Connecting = 8,
		NotRunning = 16,
		Limited = 32,
		Locked = 64
	}
}
