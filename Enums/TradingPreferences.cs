﻿using System;

namespace Z.FarmHelper.Enums {
	[Flags]
	public enum TradingPreferences : byte {
		None = 0,
		AcceptDonations = 1,
		SteamTradeMatcher = 2,
		MatchEverything = 4,
		DontAcceptBotTrades = 8,
		MatchActively = 16,
		All = AcceptDonations | SteamTradeMatcher | MatchEverything | DontAcceptBotTrades | MatchActively
	}
}
