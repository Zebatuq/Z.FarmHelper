﻿using System;

namespace Z.FarmHelper.Enums {
	[Flags]
	public enum RedeemingPreferences : byte {
		None = 0,
		Forwarding = 1,
		Distributing = 2,
		KeepMissingGames = 4,
		All = Forwarding | Distributing | KeepMissingGames
	}
}
