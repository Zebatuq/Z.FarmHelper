﻿namespace Z.FarmHelper.Enums {
	public enum SteamProtocols {
		Tcp,
		Udp,
		WebSocket,
		All
	}
}
