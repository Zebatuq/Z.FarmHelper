﻿namespace Z.FarmHelper.Enums {
	public enum PasswordFormat {
		PlainText,
		AES,
		ProtectedDataForCurrentUser
	}
}
