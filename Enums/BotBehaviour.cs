﻿using System;

namespace Z.FarmHelper.Enums {
	[Flags]
	public enum BotBehaviour : byte {
		None = 0,
		RejectInvalidFriendInvites = 1,
		RejectInvalidTrades = 2,
		RejectInvalidGroupInvites = 4,
		DismissInventoryNotifications = 8,
		MarkReceivedMessagesAsRead = 16,
		All = RejectInvalidFriendInvites | RejectInvalidTrades | RejectInvalidGroupInvites | DismissInventoryNotifications | MarkReceivedMessagesAsRead
	}
}
