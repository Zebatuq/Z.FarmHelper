﻿namespace Z.FarmHelper.Enums {
	public enum FarmingOrders : byte {
		Unordered,
		AppIDsAscending,
		AppIDsDescending,
		CardDropsAscending,
		CardDropsDescending,
		HoursAscending,
		HoursDescending,
		NamesAscending,
		NamesDescending,
		Random,
		BadgeLevelsAscending,
		BadgeLevelsDescending,
		RedeemDateTimesAscending,
		RedeemDateTimesDescending,
		MarketableAscending,
		MarketableDescending
	}
}
