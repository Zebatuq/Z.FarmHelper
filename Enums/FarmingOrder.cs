﻿namespace Z.FarmHelper.Enums {
	public enum FarmingOrder : byte {
		Unordered,
		AppIDsAscending,
		AppIDsDescending,
		CardDropsAscending,
		CardDropsDescending,
		HoursAscending,
		HoursDescending,
		NamesAscending,
		NamesDescending,
		Random,
		BadgeLevelsAscending,
		BadgeLevelsDescending,
		RedeemDateTimesAscending,
		RedeemDateTimesDescending,
		MarketableAscending,
		MarketableDescending
	}
}
