﻿namespace Z.FarmHelper.Enums {
	public enum SteamUserPermissions : byte {
		None,
		FamilySharing,
		Operator,
		Master
	}
}
