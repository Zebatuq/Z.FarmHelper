﻿namespace Z.FarmHelper.Enums {
	public enum ItemsTypes {
		Unknown,
		BoosterPack,
		Emoticon,
		FoilTradingCard,
		ProfileBackground,
		TradingCard,
		SteamGems,
		SaleItem,
		Consumable,
		ProfileModifier
	}
}
